FROM python:3 
ADD Server.py /
ADD Date.py /
RUN pip install python-socketio datetime
CMD [ "python3", "./Server.py" ]
